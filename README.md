# Blog Post #

Using HTML to create a blog post.

### Clone Repository ###

1. Fork this repository.
2. Git clone your own copy to your assignments folder.
3. Change directory into your local copy of the repo.

### To Do ###

1. Open post.html in Sublime and have a look at its structure.
2. Choose to create your own blog post or copy one from the internet.
3. Add a title for your blog post to the <head> of post.html.
4. Use <h1>, <h2>, <h3> and <p> tags to logically structure your content.
5. You can see your progress by typing in terminal 'open post.html'.